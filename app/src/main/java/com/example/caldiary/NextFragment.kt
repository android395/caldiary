package com.example.caldiary

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.caldiary.databinding.FragmentMainBinding
import com.example.caldiary.databinding.FragmentNextBinding

class NextFragment : Fragment() {

    private var binding: FragmentNextBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val fragmentBinding = FragmentNextBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            nextFragment = this@NextFragment
            lifecycleOwner = viewLifecycleOwner
        }
    }

    fun goToNextScreen() {
        findNavController().navigate(R.id.nextFragment)
    }
}