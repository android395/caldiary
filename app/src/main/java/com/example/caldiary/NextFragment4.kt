package com.example.caldiary

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.caldiary.databinding.FragmentNext3Binding
import com.example.caldiary.databinding.FragmentNext4Binding

class NextFragment4 : Fragment() {
    private var binding: FragmentNext4Binding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val fragmentBinding = FragmentNext4Binding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            nextFragment4 = this@NextFragment4
         lifecycleOwner = viewLifecycleOwner
        }
    }
//
//    fun goToNextScreen() {
//        findNavController().navigate(R.id.nextFragment4)
//    }
}