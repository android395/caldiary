package com.example.caldiary

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.caldiary.databinding.FragmentMainBinding

class MainFragment : Fragment() {
    private var binding: FragmentMainBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val fragmentBinding = FragmentMainBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            mainFragment = this@MainFragment
            lifecycleOwner = viewLifecycleOwner
        }
    }

    fun goToNextScreen1() {
        findNavController().navigate(R.id.action_mainFragment_to_nextFragment)
    }
    fun goToNextScreen2() {
        findNavController().navigate(R.id.action_mainFragment_to_nextFragment1)
    }
    fun goToNextScreen3() {
        findNavController().navigate(R.id.action_mainFragment_to_nextFragment2)
    }
    fun goToNextScreen4() {
        findNavController().navigate(R.id.action_mainFragment_to_nextFragment3)
    }
    fun goToNextScreen5() {
        findNavController().navigate(R.id.action_mainFragment_to_nextFragment4)
    }
    fun goToNextScreen6() {
        findNavController().navigate(R.id.action_mainFragment_to_nextFragment5)
    }
    fun goToNextScreen7() {
        findNavController().navigate(R.id.action_mainFragment_to_nestFragment6)
    }
}