package com.example.caldiary

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.caldiary.databinding.FragmentNext4Binding
import com.example.caldiary.databinding.FragmentNext5Binding


class NextFragment5 : Fragment() {
    private var binding: FragmentNext5Binding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val fragmentBinding = FragmentNext5Binding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            nextFragment5 = this@NextFragment5
            lifecycleOwner = viewLifecycleOwner
        }
    }
//
//    fun goToNextScreen() {
//        findNavController().navigate(R.id.nextFragment4)
//    }
}