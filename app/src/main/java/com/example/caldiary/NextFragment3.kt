package com.example.caldiary

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.caldiary.databinding.FragmentNext2Binding
import com.example.caldiary.databinding.FragmentNext3Binding


class NextFragment3 : Fragment() {
    private var binding: FragmentNext3Binding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val fragmentBinding = FragmentNext3Binding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            nextFragment3 = this@NextFragment3
            lifecycleOwner = viewLifecycleOwner
        }
    }

//    fun goToNextScreen() {
//        findNavController().navigate(R.id.nextFragment3)
//    }
}